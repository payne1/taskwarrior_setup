# Abstract

This is an attempt to document the task warrior tools I'm using, and 
possibly those that I am experimenting with.  My ultimate goal is to 
use task warrior as the central source for many todo engines, as I feel 
it makes it easier to test them out.  And some sort of collaboration 
tool (like taskarena).

# The Tools (currently using):

- task warrior
- inthe.am
    - trello sync
    - google cal sync
    - email to add task
- task warrior android app
- task whisperer
    - gnome extension provides nice at a glance view 


# On Deck:

- bugwarrior
- taskopen
- taskarena
- taskwiki
- tasksync
- vim-task
